using System.Collections.Generic;

namespace LiberaWebStore.Database {
  public interface IStorageIndexDalQueries {
    string GetIndexEntryById { get; }
    string CreateIndexEntry { get; }
    
    string GetSchemaVersion { get; }
    string SetSchemaVersion { get; }
    string CreateSchemaVersionTable { get; }
    
    Dictionary<int, string> DbSchemaMigrations { get;  }
  }
}
