﻿namespace LiberaWebStore.Database.Entities {
  public class IndexEntry {
    public string ContentId { get; set; }
    public string DownloadName { get; set; }
    public string ContentLocation { get; set; }
    public string ContentType { get; set; }
    public string ProviderType { get; set; }
  }
}
