﻿using CSharpFunctionalExtensions;
using LiberaWebStore.Storage.Structures;

namespace LiberaWebStore.Storage {
  public interface IStorageProviderFactory {
    Maybe<IStorageProvider> Create(string providerType);
    Maybe<IStorageProvider> Create(StorageProviderType providerType);
  }
}
