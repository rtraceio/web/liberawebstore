using System.Threading;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace LiberaWebStore.Database {
  public class SchemaMigrationBackgroundService : IHostedService {
    private readonly IStorageIndexDalFactory _storageIndexDalFactory;
    private readonly ILogger<SchemaMigrationBackgroundService> _logger;

    public SchemaMigrationBackgroundService(IStorageIndexDalFactory storageIndexDalFactory, ILogger<SchemaMigrationBackgroundService> logger) {
      _storageIndexDalFactory = storageIndexDalFactory;
      _logger = logger;
    }

    public Task StartAsync(CancellationToken cancellationToken) {
      _storageIndexDalFactory
        .Create()
        .RunSchemaMigration()
        .Match(
          () => _logger.LogInformation("Completed Schema Migration successfully"),
          e => _logger.LogCritical($"Schema Migration failed! {e}"));
      return Task.CompletedTask; 
    }

    public Task StopAsync(CancellationToken cancellationToken) {
      return Task.CompletedTask;
    }
  }
}
