﻿using LiberaWebStore.Storage.Structures;

namespace LiberaWebStore.Storage.Configuration {
  public class StorageProviderConfiguration {
    public StorageProviderType Provider { get; set; }
    public string DataDirectory { get; set; }
  }
}
