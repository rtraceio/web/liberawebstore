namespace LiberaWebStore.Database {
  public interface IStorageIndexDalFactory {
    IStorageIndexDal Create();
  }
}
