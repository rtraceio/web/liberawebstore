﻿using System;
using CSharpFunctionalExtensions;
using LiberaWebStore.Storage.Configuration;
using LiberaWebStore.Storage.Structures;
using Microsoft.Extensions.Options;

namespace LiberaWebStore.Storage.Implementation {
  public class StorageProviderFactory : IStorageProviderFactory {
    private readonly IOptions<StorageConfiguration> _configuration;

    public StorageProviderFactory(IOptions<StorageConfiguration> configuration) {
      _configuration = configuration;
    }

    public Maybe<IStorageProvider> Create(string providerType) {
      return Enum.TryParse<StorageProviderType>(providerType, true, out var providerTypeEnum) 
        ? Create(providerTypeEnum) 
        : Maybe<IStorageProvider>.None;
    }

    public Maybe<IStorageProvider> Create(StorageProviderType providerType) {
      var providerConfiguration = _configuration.Value.Providers
        .Find(p => p.Provider == providerType);

      if (providerConfiguration == null)
        return Maybe<IStorageProvider>.None;
      
      return providerType switch {
        StorageProviderType.Filesystem => new FilesystemStorage(providerConfiguration),
        _ => throw new ArgumentOutOfRangeException(nameof(providerType), "Identified unknown StorageProvider")
      };
    }
  }
}
