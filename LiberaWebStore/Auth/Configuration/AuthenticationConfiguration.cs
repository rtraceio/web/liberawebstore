using System.Collections.Generic;

namespace LiberaWebStore.Auth.Configuration {
  public class AuthenticationConfiguration {
    public List<string> WhitelistedApiKeys { get; set; }
  }
}
