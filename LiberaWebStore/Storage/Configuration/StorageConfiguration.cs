﻿using System.Collections.Generic;

namespace LiberaWebStore.Storage.Configuration {
  public class StorageConfiguration {
    public List<StorageProviderConfiguration> Providers { get; set; }
  }
}
