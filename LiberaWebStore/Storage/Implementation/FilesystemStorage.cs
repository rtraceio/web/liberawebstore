﻿using System;
using System.IO;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using LiberaWebStore.Storage.Configuration;

namespace LiberaWebStore.Storage.Implementation {
  public class FilesystemStorage : IStorageProvider {
    private readonly StorageProviderConfiguration _configuration;

    public FilesystemStorage(StorageProviderConfiguration configuration) {
      _configuration = configuration;
      _configuration.DataDirectory ??= Environment.CurrentDirectory;
    }

    public Result<bool> ContentExists(string contentLocation) => 
      Result.Success(File.Exists(Path.Join(_configuration.DataDirectory, contentLocation)));
    
    public Result<Maybe<byte[]>> ReadContent(string contentLocation) {
      if (!ContentExists(contentLocation).Value)
        return Result.Success(Maybe<byte[]>.None);

      try {
        var fileContent = File.ReadAllBytes(Path.Join(_configuration.DataDirectory, contentLocation));
        return Result.Success(Maybe.From(fileContent));
      } catch (Exception exc) {
        return Result.Failure<Maybe<byte[]>>($"A {typeof(IOException)} occured while trying to READ '{contentLocation}'. Hint: {exc.Message}");
      }
    }
    
    public async Task<Result<Maybe<byte[]>>> ReadContentAsync(string contentLocation) {
      if (!ContentExists(contentLocation).Value)
        return Result.Success(Maybe<byte[]>.None);

      try {
        var fileContent = await File.ReadAllBytesAsync(Path.Join(_configuration.DataDirectory, contentLocation));
        return Result.Success(Maybe.From(fileContent));
      } catch (Exception exc) {
        return Result.Failure<Maybe<byte[]>>($"A {exc.GetType().Name} occured while trying to READ from '{contentLocation}'. Hint: {exc.Message}");
      }
    }

    public Result WriteContent(string contentLocation, byte[] content, bool allowOverwrite=false) {
      if (!allowOverwrite && ContentExists(contentLocation).Value)
        return Result.Failure($"Content with Location: {contentLocation} does already exist and overwriting is disabled. Cannot Write to Location.");

      try {
        var path = Path.Join(_configuration.DataDirectory, contentLocation);
        if(File.Exists(path))
          File.Delete(path);
        File.WriteAllBytes(path, content);
        return Result.Success();
      } catch (Exception exc) {
        return Result.Failure($"A {exc.GetType().Name} occured while trying to WRITE {content.Length} bytes to '{contentLocation}'. Hint: {exc.Message}");
      }
    }
    
    public async Task<Result> WriteContentAsync(string contentLocation, byte[] content, bool allowOverwrite=false) {
      if (!allowOverwrite && ContentExists(contentLocation).Value)
        return Result.Failure($"Content with Location: {contentLocation} does already exist and overwriting is disabled. Cannot Write to Location.");

      try {
        var path = Path.Join(_configuration.DataDirectory, contentLocation);
        if(File.Exists(path))
          File.Delete(path);
        await File.WriteAllBytesAsync(path, content);
        return Result.Success();
      } catch (Exception exc) {
        return Result.Failure($"A {exc.GetType().Name} occured while trying to WRITE {content.Length} bytes to '{contentLocation}'. Hint: {exc.Message}");
      }
    }
  }
}
