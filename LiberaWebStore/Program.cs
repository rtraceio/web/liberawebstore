using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;

namespace LiberaWebStore {
  public static class Program {
    public static void Main(string[] args) {
      Host
        .CreateDefaultBuilder(args)
        .ConfigureAppConfiguration(ConfigureAppConfiguration)
        .ConfigureWebHostDefaults(webBuilder => webBuilder.UseStartup<Startup>())
        .Build()
        .Run();
    }

    private static void ConfigureAppConfiguration(HostBuilderContext context, IConfigurationBuilder builder) {
      builder
        .AddJsonFile("appsettings.json", false, true)
        .AddJsonFile("appsettings.Development.json", true, true)
        .AddJsonFile("Data/appsettings.json", true, true)
        .AddUserSecrets<Startup>(true)
        .AddEnvironmentVariables();
    }
  }
}
