﻿using System;
using System.Net;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using LiberaWebStore.Database;
using LiberaWebStore.Storage;
using LiberaWebStore.Storage.Configuration;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
// ReSharper disable InvertIf

namespace LiberaWebStore.Controllers {
  [ApiController]
  [Route("[controller]")]
  public class DownloadController : ControllerBase {
    private readonly ILogger<DownloadController> _logger;
    private readonly IOptions<StorageConfiguration> _storageConfiguration;
    private readonly IStorageProviderFactory _storageProviderFactory;
    private readonly IStorageIndexDalFactory _storageIndexDalFactory;

    public DownloadController(
      ILogger<DownloadController> logger,
      IOptions<StorageConfiguration> storageConfiguration,
      IStorageIndexDalFactory storageIndexDalFactory, 
      IStorageProviderFactory storageProviderFactory) {
      _logger = logger;
      _storageConfiguration = storageConfiguration;
      _storageIndexDalFactory = storageIndexDalFactory;
      _storageProviderFactory = storageProviderFactory;
    }

    [HttpGet("/download/{contentId}")]
    public async Task<IActionResult> Download(string contentId) {
      using var _ = _logger.BeginScope(BuildReuqestLogScope(contentId));
      var storageIndexDal = _storageIndexDalFactory.Create();
      // read from Index and abort on any kind of failure
      var (_, isIndexReadFailure, maybeIndexEntry, indexReadError) = await storageIndexDal.ReadAsync(contentId);
      if (isIndexReadFailure) {
        _logger.LogError(indexReadError);
        return StatusCode((int)HttpStatusCode.InternalServerError, indexReadError);
      }
      if (maybeIndexEntry.HasNoValue) {
        _logger.LogWarning("No IndexEntry found for {ContentId}", contentId);
        return StatusCode((int)HttpStatusCode.NotFound);
      }

      // get provider for accessing storage, abort on failure
      var storageProvider = _storageProviderFactory.Create(maybeIndexEntry.Value.ProviderType);
      if (storageProvider.HasNoValue) {
        _logger.LogError("Unknown Provider received from Index Entry. {StorageProvider} is Unknown", maybeIndexEntry.Value.ProviderType);
        return StatusCode((int)HttpStatusCode.InternalServerError);
      }

      // read content from storage provider, abort on failure
      var fileContent = await storageProvider.Value.ReadContentAsync(maybeIndexEntry.Value.ContentLocation);
      if (fileContent.IsFailure || fileContent.Value.HasNoValue) {
        _logger.LogError("Failed to read Content from Provider because {Reason}", fileContent.IsFailure ? fileContent.Error : "Source does not exist");
        return StatusCode((int)HttpStatusCode.InternalServerError);
      }
      
      return File(fileContent.Value.Value, maybeIndexEntry.Value.ContentType, maybeIndexEntry.Value.DownloadName);
    }

    private object BuildReuqestLogScope(string contentId) {
      if (HttpContext?.Request == null) return new { };

      var userAgent = HttpContext.Request.Headers["User-Agent"];

      return new {
        HttpContext.Request.QueryString,
        HttpContext.Request.Scheme,
        HttpContext.Request.ContentLength,
        HttpContext.Request.ContentType,
        HttpContext.Connection.RemoteIpAddress,
        UserAgent = userAgent,
        ContentId = contentId,
        TraceId = Guid.NewGuid().ToString()
      };
    }
  }
}
