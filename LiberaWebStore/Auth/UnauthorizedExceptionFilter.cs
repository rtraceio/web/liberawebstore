using System;
using System.Net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;

namespace LiberaWebStore.Auth {
  public class UnauthorizedExceptionFilter : IExceptionFilter {
    public void OnException(ExceptionContext context) {
      if (context.Exception.GetType() == typeof(UnauthorizedAccessException)) {
        context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
        context.HttpContext.Response.WriteAsync(context.Exception.Message);
      }
    }
  }
}
