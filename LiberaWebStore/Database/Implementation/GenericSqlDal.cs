using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using Dapper;
using LiberaWebStore.Database.Entities;
using Microsoft.Extensions.Logging;

namespace LiberaWebStore.Database.Implementation {
  public class GenericSqlDal : IStorageIndexDal {
    private readonly IStorageIndexDalQueries _queries;
    private readonly Func<IDbConnection> _connectionOpener;
    private readonly ILogger<GenericSqlDal> _logger;

    public GenericSqlDal(IStorageIndexDalQueries queries, Func<IDbConnection> connectionOpener, ILogger<GenericSqlDal> logger) {
      _queries = queries;
      _connectionOpener = connectionOpener;
      _logger = logger;
    }

    public Result RunSchemaMigration() {
      try {
        using var conn = _connectionOpener();
        try {
          using var tx = conn.BeginTransaction();
          _logger.LogInformation("Trying to create SchemaVersionTable");
          conn.Execute(_queries.CreateSchemaVersionTable);
          _logger.LogInformation("Setting SchemaVersion to initial 0");
          conn.Execute(_queries.SetSchemaVersion, new {SchemaVersion = 0});
          tx.Commit();
        } catch(Exception exc){ _logger.LogWarning(exc.Message); }
        
        var currentVersion = conn.QuerySingle<int>(_queries.GetSchemaVersion);
        var latestVersion = _queries.DbSchemaMigrations.Max(sm => sm.Key);
        if (currentVersion >= latestVersion) return Result.Success();

        var toExecute = _queries.DbSchemaMigrations
          .Where(kv => kv.Key > currentVersion)
          .ToList();
        var msg = "Starting Database Schema Migration from {FromSchemaVersion} to {ToSchemaVersion}. Executing {SchemaUpdateCount} to be on latest.";

        _logger.LogInformation(msg, currentVersion, latestVersion, toExecute.Count);

        foreach (var (version, query) in toExecute) {
          using var tx = conn.BeginTransaction();
          _logger.LogInformation("Migrating to Version {SchemaVersion}", version);
          var affectRows = conn.Execute(query);
          conn.Execute(_queries.SetSchemaVersion, new {SchemaVersion = version});
          _logger.LogInformation("Completed Migration to Version {SchemaVersion}. {AffectedRows} are affected", version, affectRows);
          tx.Commit();
        }

        return Result.Success();
      } catch (Exception exc) {
        _logger.LogCritical("Schema Migration failed because {Reason}", exc.Message);
        return Result.Failure("Schema Migration failed");
      }
    }

    public async Task<Result<IndexEntry>> CreateAsync(IndexEntry entry) {
      try {
        _logger.LogDebug("{Function} with Entry: {@Entry}; Query {Query}", nameof(CreateAsync), entry, _queries.CreateIndexEntry);
        using var conn = _connectionOpener();
        using var tx = conn.BeginTransaction();
        var rows = await conn.ExecuteAsync(_queries.CreateIndexEntry, entry);
        tx.Commit();
        return rows == 0
          ? Result.Failure<IndexEntry>("Insert Failed. 0 Rows were inserted")
          : Result.Success(entry);
      } catch (Exception exc) {
        return Result.Failure<IndexEntry>($"Could not insert because {exc.Message}");
      }
    }

    public async Task<Result<Maybe<IndexEntry>>> ReadAsync(string contentId) {
      _logger.LogDebug("{Function} with ContentId: {@ContentId}; Query {Query}", nameof(ReadAsync), contentId, _queries.GetIndexEntryById);

      try {
        using var conn = _connectionOpener();
        var queryResult = await conn.QuerySingleOrDefaultAsync<IndexEntry>(_queries.GetIndexEntryById, new {ContentId = contentId});
        return Result.Success(Maybe.From(queryResult));
      } catch (Exception exc) {
        return Result.Failure<Maybe<IndexEntry>>($"Could not query element by ContentId {contentId} because {exc.Message}");
      }
    }

    public Task<Result<IndexEntry>> DeleteAsync(string contentId) {
      throw new System.NotImplementedException();
    }
  }
}
