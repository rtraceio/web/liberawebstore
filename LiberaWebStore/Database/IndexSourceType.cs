namespace LiberaWebStore.Database {
  public enum IndexSourceType {
    Sqlite,
    MySql,
    Oracle,
    MariaDb,
    PostgreSql
  }
}
