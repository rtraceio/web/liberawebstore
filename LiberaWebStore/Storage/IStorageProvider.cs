﻿using System.Threading.Tasks;
using CSharpFunctionalExtensions;

namespace LiberaWebStore.Storage {
  public interface IStorageProvider {
    Result<bool> ContentExists(string contentLocation);
    Result<Maybe<byte[]>> ReadContent(string contentLocation);
    Task<Result<Maybe<byte[]>>> ReadContentAsync(string contentLocation);
    Result WriteContent(string contentLocation, byte[] content, bool allowOverwrite = false);
    Task<Result> WriteContentAsync(string contentLocation, byte[] content, bool allowOverwrite = false);
  }
}
