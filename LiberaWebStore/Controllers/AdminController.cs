using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using LiberaWebStore.Auth;
using LiberaWebStore.Database;
using LiberaWebStore.Database.Entities;
using LiberaWebStore.Storage;
using LiberaWebStore.Storage.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace LiberaWebStore.Controllers {
  [ApiController]
  [Route("[controller]/[action]")]
  public class AdminController : ControllerBase {
    private readonly IStorageIndexDalFactory _storageIndexDalFactory;
    private readonly IStorageProviderFactory _storageProviderFactory;
    private readonly IOptions<StorageConfiguration> _configuration;
    private readonly ILogger<AdminController> _logger;

    public AdminController(
      IStorageIndexDalFactory storageIndexDalFactory,
      IStorageProviderFactory storageProviderFactory,
      IOptions<StorageConfiguration> configuration,
      ILogger<AdminController> logger) {
      _storageIndexDalFactory = storageIndexDalFactory;
      _storageProviderFactory = storageProviderFactory;
      _configuration = configuration;
      _logger = logger;
    }

    [HttpPost]
    [ServiceFilter(typeof(ApiKeyAuthorizationFilter))]
    public async Task<IActionResult> Upload(string contentId, IFormFile file) {
      if (new[] {file?.FileName, file?.ContentType}.Any(string.IsNullOrWhiteSpace))
        return BadRequest();
      
      var indexEntry = new IndexEntry() {
        ContentId = contentId,
        ContentLocation = $"{contentId}-{file?.FileName.ToLower()}",
        ContentType = file?.ContentType,
        DownloadName = file?.FileName,
        ProviderType = _configuration.Value.Providers.First().Provider.ToString()
      };
      
      var storageProvider =_storageProviderFactory.Create(indexEntry.ProviderType);
      if (storageProvider.HasNoValue) return BadRequest();
      var memoryStream = new MemoryStream();
      await file!.CopyToAsync(memoryStream);
      var result = await storageProvider.Value
        .WriteContentAsync(indexEntry.ContentLocation, memoryStream.ToArray(), true)
        .Bind(() => _storageIndexDalFactory.Create().CreateAsync(indexEntry));

      return result.IsSuccess ? Ok(result.Value) : Conflict(result.Error);
    }
  }
}
