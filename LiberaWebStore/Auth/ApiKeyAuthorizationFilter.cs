using System;
using System.Linq;
using LiberaWebStore.Auth.Configuration;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;

// ReSharper disable InvertIf

namespace LiberaWebStore.Auth {
  public class ApiKeyAuthorizationFilter : IActionFilter {
    private readonly IOptions<AuthenticationConfiguration> _configuration;

    public ApiKeyAuthorizationFilter(IOptions<AuthenticationConfiguration> configuration) {
      _configuration = configuration;
    }
    
    public void OnActionExecuting(ActionExecutingContext context) {
      var gotHeaderValue = context.HttpContext.Request.Headers
        .TryGetValue("X-API-KEY", out var apiKeysFromHttpHeader);

      if (!gotHeaderValue || apiKeysFromHttpHeader == StringValues.Empty)
        throw new UnauthorizedAccessException("No Authentication Header found");

      if (_configuration.Value.WhitelistedApiKeys.All(apiKey => apiKey != apiKeysFromHttpHeader.FirstOrDefault()))
        throw new UnauthorizedAccessException("Unauthorized - Key not whitelisted");
    }

    public void OnActionExecuted(ActionExecutedContext context) {
      /* not implemented on purpose */
    }
  }
}
