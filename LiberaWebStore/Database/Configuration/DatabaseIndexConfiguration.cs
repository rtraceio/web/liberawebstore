namespace LiberaWebStore.Database.Configuration {
  public class DatabaseIndexConfiguration {
    public IndexSourceType IndexSource { get; set; }
    public string DatabaseTablePrefix { get; set; } = "";
    public string IndexConnectionString { get; set; }
  }
}
