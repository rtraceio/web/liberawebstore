﻿using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using LiberaWebStore.Database.Entities;

namespace LiberaWebStore.Database {
  public interface IStorageIndexDal {
    Result RunSchemaMigration();
    Task<Result<IndexEntry>> CreateAsync(IndexEntry entry);
    Task<Result<Maybe<IndexEntry>>> ReadAsync(string contentId);
    Task<Result<IndexEntry>> DeleteAsync(string contentId);
  }
}
