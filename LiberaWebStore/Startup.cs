using LiberaWebStore.Auth;
using LiberaWebStore.Auth.Configuration;
using LiberaWebStore.Database;
using LiberaWebStore.Database.Configuration;
using LiberaWebStore.Database.Implementation;
using LiberaWebStore.Storage;
using LiberaWebStore.Storage.Configuration;
using LiberaWebStore.Storage.Implementation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;


namespace LiberaWebStore {
  public class Startup {
    private IConfiguration Configuration { get; }

    public Startup(IConfiguration configuration) {
      Configuration = configuration;
    }

    public void ConfigureServices(IServiceCollection services) {
      /* Configuration */
      services.Configure<StorageConfiguration>(Configuration.GetSection(nameof(StorageConfiguration)));
      services.Configure<DatabaseIndexConfiguration>(Configuration.GetSection(nameof(DatabaseIndexConfiguration)));
      services.Configure<AuthenticationConfiguration>(Configuration.GetSection(nameof(AuthenticationConfiguration)));
      /* Startup Services */
      services.AddHostedService<SchemaMigrationBackgroundService>();

      services.AddControllers(b => {
        b.Filters.Add<UnauthorizedExceptionFilter>();
      });
      services.AddScoped<IStorageProviderFactory, StorageProviderFactory>();
      services.AddSingleton<IStorageIndexDalFactory, StorageIndexDalFactory>();
      services.AddScoped<ApiKeyAuthorizationFilter>();
      
      services.AddSwaggerGen(c => {
        c.SwaggerDoc("v1", new OpenApiInfo {
          Title = nameof(LiberaWebStore),
          Version = "v1"
        });
        c.AddSecurityDefinition("X-API-KEY", new OpenApiSecurityScheme {
          Name = "X-API-KEY",
          Type = SecuritySchemeType.ApiKey,
          In = ParameterLocation.Header
        });
        c.AddSecurityRequirement(new OpenApiSecurityRequirement {
          {
            new OpenApiSecurityScheme {
              Reference = new OpenApiReference {
                Type = ReferenceType.SecurityScheme,
                Id = "X-API-KEY"
              }
            },
            System.Array.Empty<string>()
          }
        });
      });
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
      if (env.IsDevelopment()) {
        app.UseDeveloperExceptionPage();
      }

      app.UseSwagger();
      app.UseSwaggerUI(c => {
        c.RoutePrefix = string.Empty;
        c.SwaggerEndpoint("/swagger/v1/swagger.json", $"{nameof(LiberaWebStore)} v1");
      });
      
      app.UseRouting();
      app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
    }
  }
}
