using System.Collections.Generic;
using LiberaWebStore.Database.Entities;

namespace LiberaWebStore.Database.Queries {
  public class GenericSqlQueries : IStorageIndexDalQueries {
    private const string StorageIndexTableName = "STORAGEINDEX";
    private const string SchemaVersionTableName = "SCHEMAVERSION";
    
    private readonly string _tablePrefix;

    public GenericSqlQueries(string tablePrefix) {
      _tablePrefix = tablePrefix;
    }

    public Dictionary<int, string> DbSchemaMigrations => new() {
      {0, CreateSchemaVersionTable},
      {1, CreateStorageIndexTable}
    };

    #region Migrations
    public string CreateSchemaVersionTable => 
      $"CREATE TABLE {_tablePrefix.ToUpper()}{SchemaVersionTableName} (SCHEMAVERSION INTEGER PRIMARY KEY);";

    private string CreateStorageIndexTable => @$"CREATE TABLE {_tablePrefix.ToUpper()}{StorageIndexTableName} (
              {nameof(IndexEntry.ContentId)}        VARCHAR(32)   PRIMARY KEY,
              {nameof(IndexEntry.ContentLocation)}  VARCHAR(64),
              {nameof(IndexEntry.ContentType)}      VARCHAR(32),
              {nameof(IndexEntry.DownloadName)}     VARCHAR(32),
              {nameof(IndexEntry.ProviderType)}     VARCHAR(16)
          );";

    public string SetSchemaVersion => 
      @$"INSERT INTO {_tablePrefix.ToUpper()}{SchemaVersionTableName} (SCHEMAVERSION) VALUES (:SchemaVersion)";

    public string GetSchemaVersion => $"SELECT MAX(SCHEMAVERSION) FROM {_tablePrefix.ToUpper()}{SchemaVersionTableName.ToUpper()}";

    #endregion

    public string GetIndexEntryById =>
      @$"SELECT 
        {nameof(IndexEntry.ContentId)},
        {nameof(IndexEntry.ContentLocation)},
        {nameof(IndexEntry.ContentType)},
        {nameof(IndexEntry.DownloadName)},
        {nameof(IndexEntry.ProviderType)}
       FROM {_tablePrefix.ToUpper()}{StorageIndexTableName.ToUpper()} 
       WHERE {nameof(IndexEntry.ContentId)} = :{nameof(IndexEntry.ContentId)}";

    public string CreateIndexEntry =>
      @$"INSERT INTO {_tablePrefix.ToUpper()}{StorageIndexTableName.ToUpper()} (
        {nameof(IndexEntry.ContentId)},
        {nameof(IndexEntry.ContentLocation)},
        {nameof(IndexEntry.ContentType)},
        {nameof(IndexEntry.DownloadName)},
        {nameof(IndexEntry.ProviderType)})
      VALUES (
        :{nameof(IndexEntry.ContentId)},
        :{nameof(IndexEntry.ContentLocation)},
        :{nameof(IndexEntry.ContentType)},
        :{nameof(IndexEntry.DownloadName)},
        :{nameof(IndexEntry.ProviderType)}
      )";
  }
}
