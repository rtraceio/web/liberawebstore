using System;
using System.Data;
using LiberaWebStore.Database.Configuration;
using LiberaWebStore.Database.Queries;
using Microsoft.Data.Sqlite;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using Npgsql;
using Oracle.ManagedDataAccess.Client;

namespace LiberaWebStore.Database.Implementation {
  public class StorageIndexDalFactory : IStorageIndexDalFactory {
    private readonly ILoggerFactory _loggerFactory;
    private readonly ILogger<StorageIndexDalFactory> _logger;
    private readonly IOptions<DatabaseIndexConfiguration> _configuration;
    
    public StorageIndexDalFactory(IOptions<DatabaseIndexConfiguration> configuration, ILoggerFactory loggerFactory, ILogger<StorageIndexDalFactory> logger) {
      _configuration = configuration;
      _loggerFactory = loggerFactory;
      _logger = logger;
    }

    public IStorageIndexDal Create() {
      _logger.LogInformation("Creating/Using StorageDalIndex of Type {StorageIndexDalType}", _configuration.Value.IndexSource);
      switch (_configuration.Value.IndexSource) {
        case IndexSourceType.MySql:
        case IndexSourceType.Oracle:
        case IndexSourceType.MariaDb:
        case IndexSourceType.Sqlite:
        case IndexSourceType.PostgreSql:
          return new GenericSqlDal(
            new GenericSqlQueries(_configuration.Value.DatabaseTablePrefix), 
            () => ConnectionOpen(_configuration.Value.IndexSource),
            _loggerFactory.CreateLogger<GenericSqlDal>());
        default:
          throw new ArgumentOutOfRangeException();
      }
    }
    
    private IDbConnection ConnectionOpen(IndexSourceType sourceType) {
      IDbConnection dbc = null;
      switch (sourceType) {
        case IndexSourceType.MySql:
        case IndexSourceType.MariaDb:
          dbc = new MySqlConnection(_configuration.Value.IndexConnectionString);
          break;
        case IndexSourceType.Oracle:
          dbc = new OracleConnection(_configuration.Value.IndexConnectionString);
          break;
        case IndexSourceType.PostgreSql:
          dbc = new NpgsqlConnection(_configuration.Value.IndexConnectionString);
          break;
        case IndexSourceType.Sqlite:
          dbc = new SqliteConnection(_configuration.Value.IndexConnectionString);
          break;
        default:
          throw new ArgumentOutOfRangeException(nameof(sourceType), sourceType, null);
      }
      dbc.Open();
      return dbc;
    }
  }
}
